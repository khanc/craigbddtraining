const {
  When,
  Then
} = require("cucumber");
const {
  expect
} = require("chai");
const calculateResult = require('../../src/calculate-result')

//this is where we store our result
//let result;

When("the user enters {int}", function (input) {
  //this is where we calculate whether its fiss, buzz etc
  return this.result = calculateResult(input);
});

Then("the word {string} is returned", function (expectedResult) {
  // this is where we check the result
  return expect(this.result).to.equal(expectedResult);
});