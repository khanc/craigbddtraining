const {
    After,
    Before
} = require('cucumber');

//this is used to connect to our driver application
const driver = require("selenium-webdriver");

let {
    setDefaultTimeout
} = require('cucumber');

setDefaultTimeout(60 * 1000);

Before(async function () {

    this.browser = new driver.Builder()
        //this is where we choose the browser
        .forBrowser("chrome")
        //this connects to the server that we started with npm run grid
        .usingServer("http://localhost:4444/wd/hub")
        //this sets it all up
        .build();

    return await this.browser.get("http://localhost:52330/src/index.html");
});


After(async function () {
    // This closes our browser when teh test is finished
    return await this.browser.quit();
});