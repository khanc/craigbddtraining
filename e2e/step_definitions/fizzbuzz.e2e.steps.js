const {
    When,
    Then
} = require('cucumber');
const {
    expect
} = require('chai');

const {
    By
} = require('selenium-webdriver');


When('the user enters {int}', async function (userInput) {
    //we need to enter the number into the input field
    this.browser.findElement(By.css("input#userInput")).sendKeys(userInput);

    //click submit button
    return await this.browser.findElement(By.css("input#submit")).click();

});

Then('the word {string} is returned', async function (expectedResult) {
    //find what result is displayed
    let result = await this.browser.findElement(By.css("h1#result")).getText();

    //check it against expected result
    return expect(result).to.equal(expectedResult);

});